## Установка

Чтобы все работало, нужно иметь

- python 3.9+
- libpq-dev

---

1. Установите зависимости

   ```bash
   pip3 install -r requirements.txt
   ```

2. Запустите локальный psql-сервер. Возможно, вам поможет [это](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-postgresql-on-ubuntu-18-04-ru). Достаточно дойти до шага у успешным запуском

   ```bash
   psql
   ```

3. Нужно создать бдшки, описанные в **DATABASES** в _cloud_pc/settings.py_. 

Для этого создайте `.env` файл в корне проекта. Запишите в него

* USER - имя роли
* NAME - имя БД
* PASSWORD - пароль от роли
* SECRET_KEY - рандомная строка из 50 символов
<<<<<<< HEAD

Пример (можно просто скопировать): 

   ```bash
   DEBUG=1
   NAME=example
   USER=postgres_admin
   PASSWORD=qwertyparol
   SECRET_KEY='django-insecure-hzlx721%05cw^+gb(_s)gtdh+9u!lpl00ow#&22k53^%%b71_f'
   ```

Выполните `export $(cat .env | xargs)`, чтобы загрузить переменные окружения. Затем запустите скрипт создания роли и БД:

   ```bash
   scripts/create_db.sh
   ```

5. Нужно запустить миграции

   ```bash
   ./manage.py makemigrations
   ./manage.py migrate
   ```

6. Если все хорошо, то тесты должны уметь запускаться и выдавать красивый зеленый ОК

   ```bash
   ./manage.py test
   ```

   Если все плохо, то можете попробовать написать мне ([@Moysenko](https://t.me/Moysenko)), это может быть мой косяк

7. Нужно установить pre-commit хуки, чтобы при ваших коммитах проверялся кодстайл:

   ```bash
   pre-commit install
   ```

   При `git commit ...` они автоматиески проверят внесенные изменения и отредактируют некрасивый код. Можно явно запустить их без коммита:

   - только на поменявшихся с последнего коммита файлах:
     ```bash
     pre-commit run
     ```
   - на вообще всех файлах
     ```bash
     pre-commit run --all-files
     ```

---

## Про разработку

### Добавление нового модуля

Хочется, чтобы в будущем, когда репозиторий разрастется, было удобно ориентироваться. Поэтому решил все модули (то, что создается через ./manage.py startapp ...) спрятать в services.

Чтобы добавить новый модуль (назовем его new_app), надо:

- Собственно, создать его:

  ```bash
  mkdir services/new_app
  ./manage.py startapp new_app services/new_app
  ```

- Подключить в cloud_pc/settings.py. Это делается добавлением "services.new_app" в **INSTALLED_APPS**

- В _services/new_app/apps.py_ поменять
  ```python
  name = 'new_app'
  ```
  на
  ```python
  name = 'services.new_app'
  ```

### Написание тестов

Предлагаю тесты для модуля по тем же причинам закинуть в отдельную папку _tests_. Если для теста нужны будут какие-то static-файлы, то лучше кидать их в отдельную папку для этого теста

Чтобы завести в новом модуле папку с тестами, надо

```bash
cd services/new_app
rm tests.py
mkdir tests
cd tests
touch __init__.py
mkdir static
cd ../../..
```

В итоге получается такая структура (если выкинуть всякую мишуру типа `__pycache__`)

```
.
├── README.md
├── cloud_pc
│   ├── __init__.py
│   ├── asgi.py
│   ├── settings.py
│   ├── urls.py
│   └── wsgi.py
├── create_db.sh
├── manage.py
├── requirements.txt
└── services
    ├── __init__.py
    └── example
        ├── __init__.py
        ├── admin.py
        ├── apps.py
        ├── migrations
        │   └── ...
        ├── models.py
        ├── serializers.py
        ├── tests
        │   ├── __init__.py
        │   ├── static
        │   │   └── test_car_detail
        │   │       └── expected_response.json
        │   ├── test_car_detail.py
        │   └── test_healthcheck.py
        ├── urls.py
        └── views.py
```

В _services/example/tests_ можно глянуть примеры написанных тестов. Тесты запускаются через

```bash
./manage.py test
```

### Локальный запуск

Если хочется собрать у себя бэк, можно это сделать через

```bash
./manage.py runserver 8000
```

Теперь в него можно стучаться в `http://127.0.0.1:8000` и в `http://localhost:8000`

Еще в браузере в `http://localhost:8000/admin/` можно наполнить бдшку и потестить работу ваших ручек

### Pre-commit хуки

Если во время установки вы настроили pre-commit хуки, то при коммите они могут заругаться на ваш код. В таких случаях коммит не отработает и надо будет чинить перечисленные ошибки. Некоторые ошибки будут автоматически починены, их надо будет только добавить в коммит, некоторые придется чинить руками. Если вы успешно все почините и добавите в коммит, то на этот раз он должен будет отработать :)

```bash
#small guide
pip install pre-commit
pre-commit install
# now you would able to commit only if pre-commit test passed
```

в ci используется команда pre-commit run --all-files

### Systemd

```
[Unit]
Description=schedulers_hw
After=syslog.target

[Service]
Type=forking

User=sample_user
Group=sampe_user

ExecStart=/usr/local/bin/python3.10 {path_to_project}/manage.py runserver 8000
TimeoutSec=300
SuccessExitStatus=143

[Install]
WantedBy=multi-user.target
```
#### Проверка работоспособности
```
sudo systemctl enable {service}
sudo systemctl start {service}
sudo systemctl status {service}
```
=======

Пример (можно просто скопировать): 

   ```bash
   NAME=example
   USER=postgres_admin
   PASSWORD=qwertyparol
   SECRET_KEY='django-insecure-hzlx721%05cw^+gb(_s)gtdh+9u!lpl00ow#&22k53^%%b71_f'
   ```

Выполните `export $(cat .env | xargs)`, чтобы загрузить переменные окружения. Затем запустите скрипт создания роли и БД:

   ```bash
   scripts/create_db.sh
   ```

5. Нужно запустить миграции

   ```bash
   ./manage.py makemigrations
   ./manage.py migrate
   ```

6. Если все хорошо, то тесты должны уметь запускаться и выдавать красивый зеленый ОК

   ```bash
   ./manage.py test
   ```

   Если все плохо, то можете попробовать написать мне ([@Moysenko](https://t.me/Moysenko)), это может быть мой косяк

7. Опционально, но крайне желательно. Если вы хотите, чтобы при ваших коммитах проверялся кодстайл, то установите pre-commit хуки:

   ```bash
   pre-commit install
   ```

   При `git commit ...` они автоматиески проверят внесенные изменения и отредактируют некрасивый код. Можно явно запустить их без коммита:

   - только на поменявшихся с последнего коммита файлах:
     ```bash
     pre-commit run
     ```
   - на вообще всех файлах
     ```bash
     pre-commit run --all-files
     ```

---

## Про разработку

### Добавление нового модуля

Хочется, чтобы в будущем, когда репозиторий разрастется, было удобно ориентироваться. Поэтому решил все модули (то, что создается через ./manage.py startapp ...) спрятать в services.

Чтобы добавить новый модуль (назовем его new_app), надо:

- Собственно, создать его:

  ```bash
  mkdir services/new_app
  ./manage.py startapp new_app services/new_app
  ```

- Подключить в cloud_pc/settings.py. Это делается добавлением "services.new_app" в **INSTALLED_APPS**

- В _services/new_app/apps.py_ поменять
  ```python
  name = 'new_app'
  ```
  на
  ```python
  name = 'services.new_app'
  ```

### Локальный запуск

Если хочется собрать у себя бэк, можно это сделать через

```bash
./manage.py runserver 8000
```

Теперь в него можно стучаться в `http://127.0.0.1:8000` и в `http://localhost:8000`

Еще в браузере в `http://localhost:8000/admin/` можно наполнить бдшку и потестить работу ваших ручек

### Pre-commit хуки

Если во время установки вы настроили pre-commit хуки, то при коммите они могут заругаться на ваш код. В таких случаях коммит не отработает и надо будет чинить перечисленные ошибки. Некоторые ошибки будут автоматически починены, их надо будет только добавить в коммит, некоторые придется чинить руками. Если вы успешно все почините и добавите в коммит, то на этот раз он должен будет отработать :)

```bash
#small guide
pip install pre-commit
pre-commit install
# now you would able to commit only if pre-commit test passed
```

>>>>>>> 6ba128c445a0e1c88ba85ac50ad84441f98bf30f

