#!/bin/bash

APP_NAME=$1
APP_DIR=services/$APP_NAME
APPS_PY_TEXT="
from django.apps import AppConfig\n
\n
class ExampleConfig(AppConfig):\n
    default_auto_field = 'django.db.models.BigAutoField'\n
    name = 'services.${APP_NAME}'\n
"

# Configuring app folder
mkdir services/$APP_NAME
./manage.py startapp $APP_NAME $APP_DIR
touch $APP_DIR/urls.py
touch $APP_DIR/serializers.py
echo -e $APPS_PY_TEXT > $APP_DIR/apps.py

# Configuring test folder in app
rm $APP_DIR/tests.py
mkdir $APP_DIR/tests
touch $APP_DIR/tests/__init__.py
mkdir $APP_DIR/tests/static