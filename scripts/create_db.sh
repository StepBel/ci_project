# Создаём роль для бдшек. {USER} и {PASSWORD} - USER и PASSWORD из settings.py
# psql -c "CREATE ROLE {USER} WITH SUPERUSER CREATEDB CREATEROLE LOGIN ENCRYPTED PASSWORD '{PASSWORD}'";
psql -c "CREATE ROLE $USER WITH SUPERUSER CREATEDB CREATEROLE LOGIN ENCRYPTED PASSWORD '$PASSWORD'"

# Создаем саму бд с {NAME} - NAME из settings.py 
# psql -c 'create database {NAME};'
psql -c "create database $NAME owner $USER"

