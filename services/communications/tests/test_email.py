# import json

# from django.test import TestCase

# from services.communications.models import EmailTemplate
# from services.users.models import User


# class TestEmail(TestCase):
#     def setUp(self):
#
#         my_email = 'dragun.kiu@phystech.edu'
#         User.objects.create(
#             username='TestUser',
#             email=my_email
#         )
#
#         EmailTemplate.objects.create(
#             subject='test/test_subject.html',
#             body='test/test_body.html'
#         )
#
#     def test_email(self):
#
#         REDIRECT_CODE = 302
#         USER_ID = 1
#         EMAIL_TEMPLATE_ID = 1
#
#         response = self.client.get(f'/api/communications/email/{USER_ID}/{EMAIL_TEMPLATE_ID}/')
#         with open('services/communications/tests/static/test_email/expected_response.json') as f:
#             expected_response = json.load(f)
#             self.assertEqual(response.status_code, REDIRECT_CODE)
#             self.assertEqual(response.url, expected_response)
