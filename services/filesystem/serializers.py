from rest_framework import serializers


class FileSerializer(serializers.Serializer):
    path = serializers.CharField()
    file_type = serializers.ChoiceField(choices=['DIR', 'FILE', 'LINK'])
    size = serializers.IntegerField(min_value=0)
    date_created = serializers.DateTimeField()
    date_added = serializers.DateTimeField()
    date_last_modified = serializers.DateTimeField()


class FileListRequestSerializer(serializers.Serializer):
    prefix = serializers.CharField()
    max_files = serializers.IntegerField(required=True)
    marker = serializers.CharField(allow_blank=True)


class FileListResponseSerializer(serializers.Serializer):
    files = FileSerializer(many=True)
    is_truncated = serializers.BooleanField(required=True)
    max_files = serializers.IntegerField(required=True)
    marker = serializers.CharField(allow_blank=True)
    next_marker = serializers.CharField(allow_blank=True)


class FilePathSerializer(serializers.Serializer):
    path = serializers.CharField()


class FilePatchRequestSerializer(serializers.Serializer):
    old_path = serializers.CharField()
    new_path = serializers.CharField()
