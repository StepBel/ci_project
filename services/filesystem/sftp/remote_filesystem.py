import os
from typing import List

import paramiko

from .metadata import FileMetadata, FileType, parse_file_metadata


def open_file(sftp, *args, **kwargs):
    return sftp.open(*args, **kwargs)


class RemoteFilesystem:
    def __init__(self, ip='51.250.13.144', username='administrator', port=22):
        self.ip = ip
        self.username = username
        self.port = port
        pass

    def _init_sftp(self):
        client = paramiko.SSHClient()
        client.set_missing_host_key_policy(
            paramiko.AutoAddPolicy())
        client.connect(hostname=self.ip, username=self.username,
                       port=self.port, passphrase='piZZacola239')
        return client.open_sftp()

    def listdir(self, path) -> List[FileMetadata]:
        sftp = self._init_sftp()
        attrs = sftp.listdir_attr(path)
        return [parse_file_metadata(attr, os.path.join(path, attr.filename)) for attr in attrs]

    def get_meta(self, path) -> FileMetadata:
        sftp = self._init_sftp()
        return parse_file_metadata(sftp.lstat(path), path)

    def isfile(self, path) -> bool:
        try:
            return self.get_meta(path).filetype == FileType.FILE
        except FileNotFoundError:
            return False

    def isdir(self, path) -> bool:
        try:
            return self.get_meta(path).filetype == FileType.DIR
        except FileNotFoundError:
            return False

    def remove(self, path):
        sftp = self._init_sftp()
        if self.isdir(path):
            files = self.listdir(path)
            for meta in files:
                self.remove(meta.filename)
            sftp.rmdir(path)
        else:
            sftp.remove(path)

    def makedir(self, path):
        sftp = self._init_sftp()
        sftp.mkdir(path)

    def touch(self, path):
        sftp = self._init_sftp()
        sftp.open(path, 'w').close()

    def open(self, path, mode):
        sftp = self._init_sftp()
        return sftp.open(path, mode)


def connect_to_remote(request):
    '''
    В этой функции хочется подключаться к диску пользователя
    и возвращать объект, через который можно осуществлять операции
    с удаленной файловой системой
    '''
    return RemoteFilesystem()
