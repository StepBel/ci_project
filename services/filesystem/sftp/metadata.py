import datetime
import stat
from dataclasses import dataclass
from enum import Enum, auto


class FileType(Enum):
    FILE = auto()
    DIR = auto()
    LINK = auto()
    UNKNOWN = auto()


FileTypeNames = {
    FileType.FILE: 'FILE',
    FileType.DIR: 'DIR',
    FileType.LINK: 'LINK',
    FileType.UNKNOWN: 'UNKNOWN',
}


@dataclass(init=True)
class FileMetadata:
    filetype: FileType = FileType.UNKNOWN
    size: int = 0
    creation_time: datetime.datetime = 0
    modification_time: datetime.datetime = 0
    filename: str = ''


def parse_file_metadata(attributes, filename):
    filetype = FileType.UNKNOWN

    # TODO: add links
    if stat.S_ISREG(attributes.st_mode):
        filetype = FileType.FILE
    elif stat.S_ISDIR(attributes.st_mode):
        filetype = FileType.DIR

    return FileMetadata(
        filetype=filetype,
        size=attributes.st_size,
        creation_time=datetime.datetime.fromtimestamp(attributes.st_atime),
        modification_time=datetime.datetime.fromtimestamp(attributes.st_mtime),
        filename=filename,
    )
