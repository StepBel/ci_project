import json

from drf_yasg.utils import swagger_auto_schema
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from . import serializers
from .sftp import remote_filesystem
from .sftp.metadata import FileTypeNames


class FileListView(APIView):
    permission_classes = [IsAuthenticated]

    """

    Request format:
    prefix = serializers.CharField()
    max_files = serializers.IntegerField(required=True)
    marker = serializers.CharField(allow_blank=True)
    """
    @swagger_auto_schema(
        responses={200: serializers.FileListResponseSerializer}
    )
    def get(self, request):
        path = request.GET['prefix']
        max_files = request.GET['max_files']
        max_files = int(max_files)
        is_truncated = False

        user_filesystem = remote_filesystem.connect_to_remote(request)

        files_meta = user_filesystem.listdir(path)

        objects = [{
            'path': meta.filename,
            'file_type': FileTypeNames[meta.filetype],
            'size': meta.size,
            'date_created': meta.creation_time,
            'date_added': meta.creation_time,
            'date_last_modified': meta.modification_time,
        } for meta in files_meta]

        if len(objects) > max_files:
            is_truncated = True
            objects = objects[:max_files]

        serialized_objects = serializers.FileSerializer(data=objects, many=True)
        serialized_objects.is_valid(raise_exception=True)

        response = {
            'files': list(map(dict, serialized_objects.data)),
            'is_truncated': is_truncated,
            'max_files': max_files
        }

        return Response(response)


class FileDetailView(APIView):
    permission_classes = [IsAuthenticated]

    @swagger_auto_schema(
        responses={200: serializers.FileSerializer()}
    )
    def get(self, request):
        path = request.GET['path']
        user_filesystem = remote_filesystem.connect_to_remote(request)

        meta = user_filesystem.get_meta(path)

        request_file = {
            'path': path,
            'file_type': FileTypeNames[meta.filetype],
            'size': meta.size,
            'date_created': meta.creation_time,
            'date_added': meta.creation_time,
            'date_last_modified': meta.modification_time
        }

        serialized_file_data = serializers.FileSerializer(data=request_file)
        serialized_file_data.is_valid(raise_exception=True)
        return Response(serialized_file_data.data)

    def post(self, request):
        path = request.POST['path']
        request_file = request.FILES['content']
        user_filesystem = remote_filesystem.connect_to_remote(request)

        with user_filesystem.open(path, 'wb') as file:
            file.write(request_file.read())

        return Response()

    def delete(self, request):
        request_body = json.loads(request.body.decode('utf-8').replace('\'', '"'))
        path = request_body['path']
        user_filesystem = remote_filesystem.connect_to_remote(request)
        user_filesystem.remove(path)
        return Response()


class FileRenameView(APIView):
    def post(self, request):
        path_old = request.POST['old_path']
        path_new = request.POST['new_path']
        user_filesystem = remote_filesystem.connect_to_remote(request)

        # TODO(galqiwi): change
        with user_filesystem.open(path_old, 'rb') as file:
            data = file.read()
        user_filesystem.remove(path_old)

        with user_filesystem.open(path_new, 'wb') as file:
            file.write(data)

        return Response()
