kUbuntu2004 = 'Ubuntu 20.04'
kWindows10 = 'Windows 10'
POSSIBLE_OS = (
    kUbuntu2004,
    kWindows10
)

POSSIBLE_CPU = {
    2, 4, 6, 8, 10, 12, 14,
    16, 18, 20, 22, 24, 26,
    28, 30, 32, 34, 36, 40,
    44, 48, 52, 56, 60, 64,
    68, 72, 76, 80, 84, 88,
    92, 96
}
# TODO: пока сделал просто списком, потому что непонятно, почему
# с какого-то момента идет число юнитов кратное 4. Надо будет заменить на правило

MIN_RAM = 2
MAX_RAM = 640
MIN_RAM_CPU_COEF = 1
MAX_RAM_CPU_COEF = 16

POSSIBLE_DISK_TYPES = ('HDD', 'SSD')
MIN_DISK_SPACE = {
    kUbuntu2004: 5,
    kWindows10: 50
}
MAX_DISK_SPACE = 4096
