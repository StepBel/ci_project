import json
<<<<<<< HEAD
=======
from datetime import datetime
>>>>>>> 6ba128c445a0e1c88ba85ac50ad84441f98bf30f

from django.test import TransactionTestCase
from freezegun import freeze_time

<<<<<<< HEAD
from . import utils

TEST_DIR = 'services/computers/tests/static/test_computers'
=======
from ...users.models import User
from .. import models
>>>>>>> 6ba128c445a0e1c88ba85ac50ad84441f98bf30f


class TestComputers(TransactionTestCase):
    maxDiff = None
    reset_sequences = True

    @freeze_time('2022-03-19 12:12:12')
    def setUp(self):
<<<<<<< HEAD
        self.user = utils.common_set_up()

    @freeze_time('2022-03-19 12:12:12')
    def test_get(self):
        self.client.force_login(self.user)
        response = self.client.get('/api/computers/v1/computer/1/')
        self.assertEqual(response.status_code, 200)
        with open(f'{TEST_DIR}/get_response.json') as f:
            self.assertDictEqual(json.loads(response.content), json.load(f))

    @freeze_time('2022-03-19 12:12:12')
    def test_post(self):
        self.client.force_login(self.user)
        response = self.client.post(
            '/api/computers/v1/computer/',
            {
                'owner': '1',
                'name': 'BomBomich',
                'cloud_id': 'b1gp78ncthsf17ravgac',
                'configuration': {
                    'datacenter': 'ekb',
                    'os': 'WINDOWS10',
                    'cpu': 'Intel_Core_i7_8700T_OEM',
                    'cpu_cores': 2,
                    'ram': 'Corsair_Vengeance_LPX_32GB',
                    'ram_volume': 32,
                    'storage_device': 'SAMSUNG_970_EVO_Plus_SSD_2TB',
                    'storage_device_volume': 2000,
                    'gpu': 'NVIDIA_GeForce_RTX_3060',
                }
            },
            content_type='application/json'
        )
        self.assertEqual(response.status_code, 200)
        with open(f'{TEST_DIR}/post_response.json') as f:
            self.assertDictEqual(json.loads(response.content), json.load(f))

    @freeze_time('2022-03-19 12:1:12')
    def test_delete(self):
        self.client.force_login(self.user)
        response = self.client.delete('/api/computers/v1/computer/1/')
        self.assertEqual(response.status_code, 200)
        with open(f'{TEST_DIR}/delete_response.json') as f:
            self.assertDictEqual(json.loads(response.content), json.load(f))

        response = self.client.get('/api/computers/v1/computer/1/')
        self.assertEqual(response.status_code, 404)

    def test_patch(self):
        self.client.force_login(self.user)
        response = self.client.patch(
            '/api/computers/v1/computer/1/',
            {
                'name': 'other name'
            },
            content_type='application/json',
        )
        self.assertEqual(response.status_code, 200)
        with open(f'{TEST_DIR}/patch_response.json') as f:
            self.assertDictEqual(json.loads(response.content), json.load(f))

    def test_list_computers(self):
        self.client.force_login(self.user)
        response = self.client.get(
            '/api/computers/v1/computers/',
        )
        self.assertEqual(response.status_code, 200)

        with open(f'{TEST_DIR}/bulk_get_response.json') as f:
            self.assertListEqual(json.loads(response.content), json.load(f))
=======
        user = User.objects.create(
            username='testuser',
            password='qwerty',
        )

        computer = models.Computer.objects.create(
            owner=user,
            os='WINDOWS',
            cpu='Apple M2',
            internal_memory=16,
            external_memory=256,
            location='Yekaterinburg',
            gpu='M1 Pro 14-Core GPU',
        )
        models.Status.objects.create(
            computer=computer,
            is_active=False,
            date=datetime.now()
        )

        other_computer = models.Computer.objects.create(
            os='WINDOWS',
            cpu='Apple M2',
            internal_memory=16,
            external_memory=256,
            location='Yekaterinburg',
            gpu='M1 Pro 14-Core GPU',
        )
        models.Status.objects.create(
            computer=other_computer,
            is_active=False,
            date=datetime.now()
        )

    @freeze_time('2022-03-19 12:12:12')
    def test_post(self):
        response = self.client.post(
            '/api/computers/v1/computer/',
            {
                'os': 'MACOS',
                'cpu': 'Apple M1',
                'internal_memory': 32,
                'external_memory': 512,
                'location': 'Moscow',
                'gpu': 'M1 Pro 14-Core GPU',
            }
        )
        with open('services/computers/tests/static/computer_post_response.json') as f:
            expected_response = json.load(f)
            self.assertDictEqual(response.data, expected_response)

    @freeze_time('2022-03-19 12:12:12')
    def test_get(self):
        response = self.client.get('/api/computers/v1/computer/1/')
        with open('services/computers/tests/static/computer_get_response.json') as f:
            expected_response = json.load(f)
            self.assertDictEqual(response.data, expected_response)

    @freeze_time('2022-03-19 12:12:12')
    def test_delete(self):
        response = self.client.delete('/api/computers/v1/computer/1/')
        with open('services/computers/tests/static/computer_delete_response.json') as f:
            expected_response = json.load(f)
            self.assertDictEqual(response.data, expected_response)

        response = self.client.get('/api/computers/v1/computer/1/')
        with open('services/computers/tests/static/computer_delete_response.json') as f:
            expected_response = json.load(f)
            self.assertDictEqual(response.data, expected_response)
>>>>>>> 6ba128c445a0e1c88ba85ac50ad84441f98bf30f
