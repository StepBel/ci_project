from django import forms


class ChangeUserForm(forms.Form):

    email = forms.EmailField(
        max_length=150,
        required=True,
        error_messages={
            'unique': 'A user with that email already exists.',
        },
    )
    balance = forms.IntegerField(initial=0, required=True)
    new_password = forms.CharField(max_length=128, required=False)
