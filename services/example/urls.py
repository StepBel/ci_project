from django.urls import path

from . import views

urlpatterns = [
    path('v1/car/<int:pk>/', views.CarDetail.as_view(), name='car detail'),
    path('v1/healthcheck/', views.HealthCheck.as_view(), name='v1/healthcheck/'),
]
