from rest_framework import serializers

from .models import Car, Driver


class DriverSerializer(serializers.ModelSerializer):
    class Meta:
        model = Driver
        fields = ['name', 'license']


class CarSerializer(serializers.ModelSerializer):
    class Meta:
        model = Car
        fields = ['car_model', 'release_year', 'car_number', 'owner']
