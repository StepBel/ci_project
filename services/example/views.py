from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Car, Driver
from .serializers import CarSerializer, DriverSerializer


class CarDetail(APIView):
    # pk - primary key
    def get(self, request, pk):
        owner_obj = Driver.objects.get(pk=pk)
        car_objs = Car.objects.filter(owner_id=owner_obj.id)
        response = {
            'vehicles': CarSerializer(car_objs, many=True).data,
            'drivers': DriverSerializer(owner_obj).data,
        }

        return Response(response)


class HealthCheck(APIView):
    def get(self, request):
        return Response({'ok': "It's Alive!"})
