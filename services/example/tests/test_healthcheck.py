from django.test import TestCase


class TestItsAlive(TestCase):
    def test_touch_it(self):
        response = self.client.get('/api/example/v1/healthcheck/')
        self.assertEqual(response.status_code, 200)
