from django.urls import path, re_path
from rest_auth.registration.views import ConfirmEmailView
from rest_framework.urlpatterns import format_suffix_patterns

from . import views

urlpatterns = [
    path('v1/account-confirmation-email/<str:key>/', ConfirmEmailView.as_view()),
    path('v1/', views.UserListView.as_view(), name='user_list'),
    path('v1/<int:pk>/', views.UserDetail.as_view(), name='update_user'),

    path('v1/verification-email/',
         views.CustomVerifyEmailView.as_view(), name='rest_verify_email'),
    path('v1/account-confirmation-email/',
         views.CustomVerifyEmailView.as_view(), name='account_email_verification_sent'),
    re_path(r'^account-confirmation-email/(?P<key>[-:\w]+)/$',
            views.CustomVerifyEmailView.as_view(), name='account_confirm_email'),
    path('v1/password/change/', views.CustomPasswordChangeView.as_view(),
         name='rest_custom_password_change'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
