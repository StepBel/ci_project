from datetime import timedelta

from allauth.account import app_settings as allauth_settings
from allauth.account.adapter import get_adapter
from allauth.account.utils import setup_user_email
from allauth.utils import email_address_exists
from django.conf import settings
from django.db.models import Min
from django.utils.encoding import force_text
from django.utils.http import base36_to_int
from django.utils.http import urlsafe_base64_decode as uid_decoder
from django.utils.translation import ugettext_lazy as _
from rest_auth.serializers import (PasswordResetConfirmSerializer,
                                   PasswordResetSerializer)
from rest_framework import exceptions, serializers
from rest_framework.exceptions import ValidationError

from services.users.forms import CustomSetPasswordForm
from services.users.models import (Tokens, User, from_timedelta, to_timedelta,
                                   truncate_sent_counter)


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'


class CustomRegisterSerializer(serializers.Serializer):

    email = serializers.EmailField()

    def validate_email(self, email):
        email = get_adapter().clean_email(email)
        if allauth_settings.UNIQUE_EMAIL:
            if email and email_address_exists(email):
                raise serializers.ValidationError(
                    _('A user is already registered with this e-mail address.'))
        return email

    def get_cleaned_data(self):
        return {
            'email': self.validated_data.get('email', '')
        }

    def save(self, request):
        adapter = get_adapter()
        user = adapter.new_user(request)
        self.cleaned_data = self.get_cleaned_data()
        adapter.save_user(request, user, self)
        setup_user_email(request, user, [])

        return user


class CustomPasswordChangeSerializer(serializers.Serializer):

    new_password1 = serializers.CharField(max_length=128)
    set_password_form_class = CustomSetPasswordForm

    def __init__(self, *args, **kwargs):

        self.logout_on_password_change = getattr(
            settings, 'LOGOUT_ON_PASSWORD_CHANGE', False
        )
        super(CustomPasswordChangeSerializer, self).__init__(*args, **kwargs)

        self.request = self.context.get('request')
        self.user = getattr(self.request, 'user', None)

    def validate(self, attrs):
        self.set_password_form = self.set_password_form_class(
            user=self.user, data=attrs
        )

        if not self.set_password_form.is_valid():
            raise serializers.ValidationError(self.set_password_form.errors)
        return attrs

    def save(self):
        self.set_password_form.save()
        if not self.logout_on_password_change:
            from django.contrib.auth import update_session_auth_hash
            update_session_auth_hash(self.request, self.user)


class CustomPasswordResetSerializer(PasswordResetSerializer):
    def get_email_options(self):
        email = self.reset_form.cleaned_data['email']
        new_token = Tokens(email=email, time=to_timedelta())
        new_token.save()

        return {
            'from_email': 'CloudPC@yandex.ru',
            'domain_override': '127.0.0.1:8000',
            'email_template_name': 'email/registration/password_reset_email.html'
        }

    def validate_email(self, value):
        # Create PasswordResetForm with the serializer
        self.reset_form = self.password_reset_form_class(data=self.initial_data)
        if not self.reset_form.is_valid():
            raise serializers.ValidationError(self.reset_form.errors)

        truncate_sent_counter()
        email = self.reset_form.cleaned_data['email']
        time_bound = to_timedelta() - settings.TOKEN_PERIOD
        email_filtered = Tokens.objects.filter(email__exact=email)
        active_tokens = email_filtered.filter(time__gt=time_bound).count()

        if active_tokens > settings.MAX_TOKEN_NUM:
            minimal_time = Tokens.objects.aggregate(Min('time'))['time__min']
            next_token_time = minimal_time + settings.TOKEN_PERIOD
            time_str = from_timedelta(next_token_time).strftime('%m/%d/%Y, %H:%M:%S')
            next_time = f'Try again no sooner than at {time_str} GMT+3'
            raise serializers.ValidationError('Confirmation mail limit exceeded. ' + next_time)

        return value

    def save(self):

        request = self.context.get('request')

        opts = {
            'use_https': True,
            'from_email': getattr(settings, 'DEFAULT_FROM_EMAIL'),
            'request': request,
        }

        opts.update(self.get_email_options())
        self.reset_form.save(**opts)


class CustomPasswordResetConfirmSerializer(PasswordResetConfirmSerializer):
    def custom_validation(self, attrs):

        # Decode the uidb64 to uid to get User object
        try:
            uid = force_text(uid_decoder(attrs['uid']))
            self.user = User._default_manager.get(pk=uid)
        except (TypeError, ValueError, OverflowError, User.DoesNotExist):
            raise ValidationError({'uid': ['Invalid value']})

        token = attrs['token']

        # Parse the token
        try:
            ts_b36, _ = token.split('-')
        except ValueError:
            return False

        try:
            time_stamp = base36_to_int(ts_b36)
        except ValueError:
            return False

        truncate_sent_counter()
        now = to_timedelta() + timedelta(seconds=30)
        delta = now - timedelta(seconds=time_stamp)
        extra_delta = timedelta(hours=1)

        if delta + extra_delta > settings.TOKEN_LIFETIME:

            uid = force_text(uid_decoder(attrs['uid']))
            user = User._default_manager.get(pk=uid)

            email = user.email
            active_tokens = Tokens.objects.filter(email__exact=email).count()

            if active_tokens:
                raise ValidationError({'token': ['Token expired.' +
                                                 ' Please, use another token.']})

            # TODO: Send verification email
            raise ValidationError({'token': ['Token expired.' +
                                             ' New validation link has just been sent.']})

        return attrs


class CustomLoginSerializer(serializers.Serializer):
    email = serializers.EmailField(required=False, allow_blank=True)
    password = serializers.CharField(style={'input_type': 'password'})

    def authenticate(self, email, password):
        try:
            user = User._default_manager.get(email=email)
        except User.DoesNotExist:
            msg = _('User with this email is not registrated.')
            raise exceptions.ValidationError(msg)
        if user.check_password(password):
            return user
        msg = _('Incorrect password.')
        raise exceptions.ValidationError(msg)

    def _validate_email(self, email, password):
        user = None

        if email and password:
            user = self.authenticate(email=email, password=password)
        else:
            msg = _('Must include "email" and "password".')
            raise exceptions.ValidationError(msg)

        return user

    def validate(self, attrs):
        email = attrs.get('email')
        password = attrs.get('password')

        # Authentication through email
        user = self._validate_email(email, password)

        # Did we get back an active user?
        if user:
            if not user.is_active:
                msg = _('User account is disabled.')
                raise exceptions.ValidationError(msg)
        else:
            msg = _(f'Unable to log in with provided credentials. {User.USERNAME_FIELD}')
            raise exceptions.ValidationError(msg)

        # If required, is the email verified?
        # if 'rest_auth.registration' in settings.INSTALLED_APPS:
        #     if allauth_settings.EMAIL_VERIFICATION == \
        #             allauth_settings.EmailVerificationMethod.MANDATORY:
        #         email_address = user.emailaddress_set.get(email=user.email)
        #         if not email_address.verified:
        #             raise serializers.ValidationError(_('E-mail is not verified.'))

        attrs['user'] = user
        return attrs
