import json

from django.test import TestCase

from services.users.models import User


class TestUsersList(TestCase):
    def setUp(self):
        User.objects.all().delete()
        User.objects.create(
            username='Misha',
            email='some_mail@yandex.ru'
        )
        User.objects.create(
            username='Oleg',
            email='another_mail@yandex.ru'
        )

        User.objects.create(
            username='Kirill',
            email='kirill1997@google.com'
        )

    def test_users_list(self):
        response = self.client.get('/api/users/v1/')
        with open('services/users/tests/static/test_users_list/expected_response.json') as f:
            expected_response = json.load(f)
            received = json.loads(response.content)
            self.assertEqual(brief(received), expected_response)

    def test_get_user(self):
        id = get_field(self, 0, 'id')
        response = self.client.get(f'/api/users/v1/{id}/')
        with open('services/users/tests/static/test_get_user/expected_response.json') as f:
            expected_response = json.load(f)[0]
            received = json.loads(response.content)
            self.assertEqual(brief(received), expected_response)

    def test_update_username(self):
        id = get_field(self, 1, 'id')
        response = self.client.patch(f'/api/users/v1/{id}/', json.dumps({
            'username': 'updated_username'
        }),
            content_type='application/json')
        with open('services/users/tests/static/test_get_user/expected_response.json') as f:
            expected_response = json.load(f)[1]
            received = json.loads(response.content)
            self.assertEqual(brief(received), expected_response)

    def test_update_email(self):
        id = get_field(self, 2, 'id')
        response = self.client.patch(f'/api/users/v1/{id}/', json.dumps({
            'email': 'updated_mail@yandex.ru'
        }),
            content_type='application/json')
        with open('services/users/tests/static/test_get_user/expected_response.json') as f:
            expected_response = json.load(f)[2]
            received = json.loads(response.content)
            self.assertEqual(brief(received), expected_response)

    def test_delete_user(self):
        id = get_field(self, 0, 'id')
        response = self.client.delete(f'/api/users/v1/{id}/')
        not_found = {'detail': 'Not found.'}
        self.assertEqual(response.status_code, 204)

        response = self.client.get(f'/api/users/v1/{id}/')
        self.assertEqual(json.loads(response.content), not_found)


def brief(full_info):
    def get_short(user): return {'username': user['username'], 'email': user['email']}
    if type(full_info) is list:
        shortened = [get_short(user) for user in full_info]
    else:
        shortened = get_short(full_info)
    return shortened


def get_field(test, num, field):
    response_all = json.loads(test.client.get('/api/users/v1/').content)
    return response_all[num][f'{field}']
