import json

from django.test import TestCase

from services.users.models import User


class TestRegistration(TestCase):

    def test_registration(self):
        test_email = 'mymail@mail.ru'
        response = self.client.post('/api/auth/registration/', {
            'email': test_email,
        })

        key = json.loads(response.content)
        self.assertEqual([k for k in key], ['detail'])
        self.assertEqual(1, User.objects.filter(email__exact=test_email).count())
