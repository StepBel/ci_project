# from allauth.account import app_settings
# from allauth.account.adapter import get_adapter
# from django.core import signing
# from django.test import TestCase
# from django.urls import reverse

# class TestSignupConfirm(TestCase):
#     def setUp(self):
#
#         self.test_email = 'petrov.ab@phystech.edu'
#         self.client.post('/api/auth/registration/', {
#             'email': self.test_email,
#         })
#
#     def test_signup_confirm(self):
#
#         if app_settings.EMAIL_CONFIRMATION_HMAC:
#             key = signing.dumps(obj=1, salt=app_settings.SALT)
#         else:
#             key = get_adapter().generate_emailconfirmation_key(self.test_email)
#
#         url = reverse('rest_verify_email')
#         print('URL:', url)
#
#         response = self.client.post(url, {
#             'key': str(key),
#         })
#         self.assertEqual(response.content, b'')
#         self.assertRedirects(response, '/api/users/v1/password/change/')
#
#         response = self.client.post('/api/users/v1/password/change/', {
#             'password1': 'its_a_password',
#         })
#         print(response.content)
#         self.assertEqual(response.content, b'')
