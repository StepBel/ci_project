import os

from allauth.account.adapter import DefaultAccountAdapter
from django.conf import settings
from django.contrib.sites.shortcuts import get_current_site


class CustomAdapter(DefaultAccountAdapter):

    def send_confirmation_mail(self, request, emailconfirmation, signup):
        current_site = get_current_site(request)
        activate_url = self.get_email_confirmation_url(request, emailconfirmation)
        ctx = {
            'user': emailconfirmation.email_address.user,
            'activate_url': activate_url,
            'current_site': current_site,
            'key': emailconfirmation.key,
        }
        print(emailconfirmation.key)

        templates_path = os.path.join(*[settings.BASE_DIR, 'templates', 'email'])
        if signup:
            email_template = os.path.join(templates_path, 'email_confirmation_signup')
        else:
            email_template = os.path.join(templates_path, 'email_confirmation')

        self.send_mail(email_template, emailconfirmation.email_address.email, ctx)
