from rest_framework import serializers


class PipelineExecutionRequestSerializer(serializers.Serializer):
    pipeline_key = serializers.CharField()
    pipeline_kwargs = serializers.JSONField(allow_null=True)
    starting_task_number = serializers.IntegerField(allow_null=True)
