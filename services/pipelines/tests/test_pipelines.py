import threading
import time

from django.test import TransactionTestCase

from ..utils.pipeline import Pipeline
from ..utils.tasks import Task

TICK_LENGTH = 0.1
mutex = threading.Lock()


class TestPipelines(TransactionTestCase):
    def test_task(self):
        class TestTask(Task):
            def run(self, kwargs):
                kwargs['counter'][0] += 1
                return kwargs

        task = TestTask()

        counter = [0]

        self.assertEqual(counter[0], 0)
        task.run({'counter': counter})
        self.assertEqual(counter[0], 1)

        pipeline = Pipeline([task, task])
        pipeline.run({'counter': counter})
        self.assertEqual(counter[0], 3)

    def test_detach(self):

        class TestTask(Task):
            def run(self, kwargs):
                time.sleep(TICK_LENGTH * 2)
                mutex.acquire()
                kwargs['counter'][0] += 1
                mutex.release()
                return kwargs

        task = TestTask()

        counter = [0]

        pipeline = Pipeline([task, task])
        pipeline.run_detached({'counter': counter})
        mutex.acquire()
        self.assertEqual(counter[0], 0)
        mutex.release()

        time.sleep(TICK_LENGTH * 3)
        mutex.acquire()
        self.assertEqual(counter[0], 1)
        mutex.release()

        time.sleep(TICK_LENGTH * 2)
        mutex.acquire()
        self.assertEqual(counter[0], 2)
        mutex.release()

    def test_detach_2(self):

        class TestTask(Task):
            def run(self, kwargs):
                time.sleep(TICK_LENGTH * 2)
                mutex.acquire()
                kwargs['counter'][0] += 1
                mutex.release()
                return kwargs

        task = TestTask()

        counter = [0]

        pipeline = Pipeline([task, task])
        pipeline.run_detached({'counter': counter})
        pipeline.run_detached({'counter': counter})
        mutex.acquire()
        self.assertEqual(counter[0], 0)
        mutex.release()

        time.sleep(TICK_LENGTH * 3)
        mutex.acquire()
        self.assertEqual(counter[0], 2)
        mutex.release()

        time.sleep(TICK_LENGTH * 2)
        mutex.acquire()
        self.assertEqual(counter[0], 4)
        mutex.release()

    def test_faulty(self):
        class TestTask(Task):
            def run(self, kwargs):
                time.sleep(TICK_LENGTH)
                kwargs['counter'][0] += 1
                if kwargs['counter'][0] < 3:
                    raise Exception()

                return kwargs

        task = TestTask()

        counter = [0]

        pipeline = Pipeline([task, task])
        pipeline.run({'counter': counter})
        self.assertEqual(4, counter[0])
