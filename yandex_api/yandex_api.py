import json
import os
import time
import uuid
from dataclasses import dataclass
from enum import Enum, auto
from functools import cached_property
from typing import Optional

import jwt
import requests


def get_nested_dict_or_none(value, args):
    if len(args) == 0:
        return value

    if args[0] in value:
        return get_nested_dict_or_none(value[args[0]], args[1:])
    else:
        return None


class YandexAPIException(Exception):
    pass


class PCType(Enum):
    WINDOWS = auto()
    LINUX = auto()
    CLONE = auto()


@dataclass(frozen=False)
class PCConfiguration:
    memory: int = 2 * 1024 ** 3
    cores: int = 2
    disk_size: int = 60 * 1024 ** 3
    type: PCType = PCType.WINDOWS
    diskId: str = ''  # makes sense only if PCType==CLONE
    zoneId: str = 'ru-central1-a'
    name_prefix: str = ''


@dataclass(frozen=False)
class PCState:
    diskId: str
    ipv4: str
    status: str


class YandexApiSession:
    def __init__(self):
        pass

    def _wait_for_operation(self, operation_id, time_delta=0.5):
        while True:
            response = requests.get(
                f'https://operation.api.cloud.yandex.net/operations/{operation_id}',
                headers={
                    'Content-Type': 'application/json',
                    'Authorization': f'Bearer {self._iam}'
                },
            )

            if response.status_code != 200:
                raise YandexAPIException(response.text)

            if response.json()['done']:
                break

            time.sleep(time_delta)

    @cached_property
    def _iam(self) -> str:
        robot_key = json.loads(os.environ['YANDEX_API_KEY'])
        service_account_id = robot_key['service_account_id']
        key_id = robot_key['id']
        private_key = robot_key['private_key']

        now = int(time.time())
        payload = {
            'aud': 'https://iam.api.cloud.yandex.net/iam/v1/tokens',
            'iss': service_account_id,
            'iat': now,
            'exp': now + 3600}

        encoded_token = jwt.encode(
            payload,
            private_key,
            algorithm='PS256',
            headers={'kid': key_id})

        response = requests.post(
            'https://iam.api.cloud.yandex.net/iam/v1/tokens',
            headers={
                'Content-Type': 'application/json',
            }, data=json.dumps({
                'jwt': encoded_token,
            })
        )

        if response.status_code != 200:
            raise YandexAPIException(response.text)

        return response.json()['iamToken']

    @cached_property
    def _cloud_id(self) -> str:
        response = requests.get(
            'https://resource-manager.api.cloud.yandex.net/resource-manager/v1/clouds',
            headers={
                'Authorization': f'Bearer {self._iam}'
            }
        )

        if response.status_code != 200:
            raise YandexAPIException(response.text)

        clouds = response.json()['clouds']

        for cloud in clouds:
            if cloud['name'] == 'cloudpc':
                return cloud['id']

        raise YandexAPIException('cloudpc cloud is not foud')

    @cached_property
    def _folder_id(self) -> str:
        response = requests.get(
            'https://resource-manager.api.cloud.yandex.net/resource-manager/v1/folders',
            headers={
                'Authorization': f'Bearer {self._iam}',
            }, params={
                'cloudId': self._cloud_id,
            }
        )

        if response.status_code != 200:
            raise YandexAPIException(response.text)

        folders = response.json()['folders']

        if len(folders) > 1:
            raise YandexAPIException('Got more than one folder, expected one')

        return folders[0]['id']

    def create_computer(self, conf: PCConfiguration) -> str:
        disk_spec = {
            'size': str(conf.disk_size),
        }
        if conf.type == PCType.CLONE:
            disk_spec['snapshotId'] = conf.snapshotId
        else:
            disk_spec['imageId'] = 'fd8jktv9khv0d23gvmt6'

        request_config = {
            'folderId': self._folder_id,
            'Name': f'{conf.name_prefix}+{uuid.uuid4()}',
            'zoneId': conf.zoneId,
            'platformId': 'standard-v1',
            'resourcesSpec': {
                'memory': str(conf.memory),
                'cores': str(conf.cores),
            },
            'bootDiskSpec': {
                'autoDelete': False,
                'diskSpec': disk_spec,
            },
            'networkInterfaceSpecs': {
                'subnetId': 'e9bse0br55cfmkebkbdk',
                'primaryV4AddressSpec': {
                    'oneToOneNatSpec': {
                        'ipVersion': 'IPV4',
                    }
                }
            }
        }

        response = requests.post(
            'https://compute.api.cloud.yandex.net/compute/v1/instances',
            headers={
                'Content-Type': 'application/json',
                'Authorization': f'Bearer {self._iam}'
            },
            data=json.dumps(request_config)
        )

        if response.status_code != 200:
            raise YandexAPIException(response.text)

        self._wait_for_operation(response.json()['id'])

        return response.json()['metadata']['instanceId']

    def delete_computer(self, computer_id: str):
        response = requests.delete(
            f'https://compute.api.cloud.yandex.net/compute/v1/instances/{computer_id}',
            headers={
                'Content-Type': 'application/json',
                'Authorization': f'Bearer {self._iam}'
            }
        )

        if response.status_code != 200:
            raise YandexAPIException(response.text)

        self._wait_for_operation(response.json()['id'])

    def start_computer(self, computer_id: str):
        response = requests.post(
            f'https://compute.api.cloud.yandex.net/compute/v1/instances/{computer_id}:start',
            headers={
                'Content-Type': 'application/json',
                'Authorization': f'Bearer {self._iam}'
            }
        )

        if response.status_code != 200:
            raise YandexAPIException(response.text)

        self._wait_for_operation(response.json()['id'])

    def stop_computer(self, computer_id: str):
        response = requests.post(
            f'https://compute.api.cloud.yandex.net/compute/v1/instances/{computer_id}:stop',
            headers={
                'Content-Type': 'application/json',
                'Authorization': f'Bearer {self._iam}'
            }
        )

        if response.status_code != 200:
            raise YandexAPIException(response.text)

        self._wait_for_operation(response.json()['id'])

    def get_computer_state(self, computer_id: str) -> PCState:
        response = requests.get(
            f'https://compute.api.cloud.yandex.net/compute/v1/instances/{computer_id}',
            headers={
                'Content-Type': 'application/json',
                'Authorization': f'Bearer {self._iam}'
            }
        )

        if response.status_code != 200:
            raise YandexAPIException(response.text)

        state_raw = response.json()

        network_interfaces = state_raw['networkInterfaces']
        if len(network_interfaces) != 1:
            raise YandexAPIException(response.text)

        return PCState(
            diskId=get_nested_dict_or_none(
                state_raw,
                ['bootDisk', 'diskId']
            ),
            ipv4=get_nested_dict_or_none(
                state_raw,
                ['networkInterfaces', 0, 'primaryV4Address', 'oneToOneNat', 'address']
            ),
            status=state_raw['status'],
        )

    # I don't think we really need this method
    def create_snapshot(self, computer_id: str) -> Optional[str]:
        state = self.get_computer_state(computer_id)
        if state.diskId is None:
            return None

        response = requests.post(
            'https://compute.api.cloud.yandex.net/compute/v1/snapshots',
            headers={
                'Content-Type': 'application/json',
                'Authorization': f'Bearer {self._iam}'
            },
            data=json.dumps({
                'folderId': self._folder_id,
                'diskId': state.diskId,
            }),
        )

        if response.status_code != 200:
            raise YandexAPIException(response.text)

        self._wait_for_operation(response.json()['id'])

        return response.json()['metadata']['snapshotId']

    # I don't think we really need this method
    def delete_snapshot(self, snapshot_id: str):
        response = requests.delete(
            f'https://compute.api.cloud.yandex.net/compute/v1/snapshots/{snapshot_id}',
            headers={
                'Content-Type': 'application/json',
                'Authorization': f'Bearer {self._iam}'
            },
        )

        if response.status_code != 200:
            raise YandexAPIException(response.text)

        self._wait_for_operation(response.json()['id'])

    def attach_disk(self, computer_id, disk_id):
        response = requests.post(
            f'https://compute.api.cloud.yandex.net/compute/v1/instances/{computer_id}:attachDisk',
            headers={
                'Content-Type': 'application/json',
                'Authorization': f'Bearer {self._iam}'
            },
            data=json.dumps({
                'attachedDiskSpec': {
                    'diskId': disk_id,
                }
            })
        )

        if response.status_code != 200:
            raise YandexAPIException(response.text)

        self._wait_for_operation(response.json()['id'])

    def detach_disk(self, computer_id, disk_id):
        response = requests.post(
            f'https://compute.api.cloud.yandex.net/compute/v1/instances/{computer_id}:detachDisk',
            headers={
                'Content-Type': 'application/json',
                'Authorization': f'Bearer {self._iam}'
            },
            data=json.dumps({
                'diskId': disk_id,
            })
        )

        if response.status_code != 200:
            raise YandexAPIException(response.text)

        self._wait_for_operation(response.json()['id'])
