"""cloud_pc URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.contrib import admin
from django.urls import include, path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_auth.views import PasswordResetConfirmView, PasswordResetView
from rest_framework import permissions

from services.example.models import Car, Driver
from services.users.views import CustomRegisterView

admin.site.register(Car)
admin.site.register(Driver)


schema_view = get_schema_view(
    openapi.Info(**settings.SWAGGER_API_INFO),
    public=True,
    permission_classes=[permissions.AllowAny],
)


urlpatterns = [
    path('admin/', admin.site.urls),
    path('swagger/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('api/example/', include('services.example.urls')),
    path('api/computers/', include('services.computers.urls')),
    path('adminpage/', include('services.adminpage.urls')),
    path('api/filesystem/', include('services.filesystem.urls')),
    # Auth urls - TODO refactor
    path('api/users/', include('services.users.urls')),
    path('api/communications/', include('services.communications.urls')),
    path('api/auth/', include('rest_auth.urls')),
    path('api/auth/registration/', CustomRegisterView.as_view(), name='rest_register'),
    path('api/auth/reset/', PasswordResetView.as_view()),
    path('api/auth/confirmation/<uidb64>/<token>/',
         PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    path('api/pipelines/', include('services.pipelines.urls'))
]
